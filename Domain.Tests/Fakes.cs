﻿namespace Domain.Tests
{
    public static class Fakes
    {
        public static Group BuildGroup(string name = "Group", double maxCapacity = 100)
            => new Group(Name.Create(name), Capacity.InAmps(maxCapacity));

        public static Group WithStation(this Group group,
            string name = "Station", params double[] connectorCapacities)
        {
            group.AddStation(
                new StationConfiguration(name, connectorCapacities ?? new double[] { 10 }));
            return group;
        }

        public static ChargeStation BuildStation(
          string name = "Station",
          params double[] connectorCapacities)
        {
            return new ChargeStation(
                Name.Create(name),
                connectorCapacities);
        }

        public static Connector BuildConnector(int id, double capacityInAmp)
            => new Connector(id, capacityInAmp);
    }
}