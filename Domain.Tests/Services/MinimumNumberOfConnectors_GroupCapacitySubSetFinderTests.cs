﻿using Domain.Services;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Tests
{
    [TestClass]
    public class MinimumNumberOfConnectors_GroupCapacitySubSetFinderTests
    {
        private MinimumNumberOfConnectors_GroupCapacitySubSetFinder _finder;

        [TestInitialize]
        public void Initialize()
        {
            _finder = new MinimumNumberOfConnectors_GroupCapacitySubSetFinder();
        }

        [TestMethod]
        public void Able_to_find_single_connector_for_targeted_capacity()
        {
            var targetCapacity = 15;
            var group = Fakes.BuildGroup("Test", 1000)
                .WithStation("Station1", connectorCapacities: new double[] { 5, 10, 15 })
                .WithStation("Station2", connectorCapacities: new double[] { 20, 40, 70 })
                .WithStation("Station3", connectorCapacities: new double[] { 55, 55, 55 });

            var expectedCombinations = new List<List<StationConnector>>()
            {
                 new List<StationConnector>{
                     new StationConnector(group.ChargeStations.First(),new Connector(id: 3, maxAmps: 15)) },
            };

            var actualCombinations = _finder.FindConnectorSubSets(
                group, Capacity.InAmps(targetCapacity));

            actualCombinations.Should().BeEquivalentTo(expectedCombinations);
        }

        [TestMethod]
        public void Able_to_find_multiple_connector_for_targeted_capacity()
        {
            var targetCapacity = 55;
            var group = Fakes.BuildGroup("Test", 1000)
                .WithStation("Station1", connectorCapacities: new double[] { 20, 55, 70 })
                .WithStation("Station2", connectorCapacities: new double[] { 55, 55, 55 });

            var expectedCombinations = new List<List<StationConnector>>()
            {
                 new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.First(),new Connector(id: 2, maxAmps: 55)) },

                 new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 1, maxAmps: 55)) },

                 new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 2, maxAmps: 55)) },

                 new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 3, maxAmps: 55)) }
            };

            var actualCombinations = _finder.FindConnectorSubSets(
               group, Capacity.InAmps(targetCapacity));

            actualCombinations.Should().BeEquivalentTo(expectedCombinations);
        }

        [TestMethod]
        public void Able_to_find_multiple_combinations_for_targeted_capacity()
        {
            var targetCapacity = 55;
            var group = Fakes.BuildGroup("Test", 1000)
                .WithStation("Station1", connectorCapacities: new double[] { 25, 10, 10, 15 })
                .WithStation("Station2", connectorCapacities: new double[] { 25, 40, 35, 30, 20 });

            var expectedCombinations = new List<List<StationConnector>>()
            {
                 new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.First(),new Connector(id: 4, maxAmps: 15)),
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 2, maxAmps: 40))
                 },
                  new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.First(),new Connector(id: 1, maxAmps: 25)),
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 4, maxAmps: 30))
                 },
                  new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 1, maxAmps: 25)),
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 4, maxAmps: 30))
                 },
                  new List<StationConnector>(){
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 5, maxAmps: 20)),
                     new StationConnector(group.ChargeStations.Last(),new Connector(id: 3, maxAmps: 35))
                 }
            };

            var actualCombinations = _finder.FindConnectorSubSets(
               group, Capacity.InAmps(targetCapacity));

            actualCombinations.Should().BeEquivalentTo(expectedCombinations);
        }

        [TestMethod]
        [DataRow(45, 2)]
        [DataRow(55, 4)]
        [DataRow(30, 1)]
        [DataRow(91, 1)]
        [DataRow(13, 0)]
        public void Able_to_find_optimal_combinations_of_connectors_per_target_capacity(
            int targetCapacity, int expectedCombinationsCount)
        {
            var group = Fakes.BuildGroup("Test", 1000)
                .WithStation("Station1", connectorCapacities: new double[] { 5, 10, 15 })
                .WithStation("Station2", connectorCapacities: new double[] { 20, 40, 55 })
                .WithStation("Station3", connectorCapacities: new double[] { 30, 60, 70 })
                .WithStation("Station4", connectorCapacities: new double[] { 90, 51, 67 })
                .WithStation("Station5", connectorCapacities: new double[] { 55, 55, 55 });

            var actualCombinations = _finder.FindConnectorSubSets(group, Capacity.InAmps(targetCapacity));

            actualCombinations.Count
                .Should()
                .Be(expectedCombinationsCount);
        }
    }
}