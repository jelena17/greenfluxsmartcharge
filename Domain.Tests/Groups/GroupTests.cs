﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using static Domain.ErrorMessage;

namespace Domain.Tests
{
    [TestClass]
    public class GroupTests
    {
        [TestMethod]
        [DataRow("TestGroup", 12)]
        [DataRow("Test_1_2_3", 1.20)]
        public void Albe_to_create_new_group(string name, double amps)
        {
            var groupName = Name.Create(name);
            var group = new Group(groupName, Capacity.InAmps(amps));

            group.Name
                .Should()
                .Be(groupName);

            group.MaxCapacity.Value
                .Should()
                .Be(amps);
        }

        [TestMethod]
        public void Albe_to_update_group_capacity()
        {
            var group = Fakes.BuildGroup(maxCapacity: 50);

            group.ChangeGroupCapacity(Capacity.InAmps(1000));

            group.MaxCapacity.Value
                .Should()
                .Be(1000);
        }

        [TestMethod]
        public void Throw_error_decreasing_group_capacity_when_it_is_allocated()
        {
            var group = Fakes
                .BuildGroup(maxCapacity: 50)
                .WithStation(connectorCapacities: new double[] { 10, 20 });

            Action action = () => group.ChangeGroupCapacity(Capacity.InAmps(20));

            action
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(Insuficient_Group_Capacity);
        }

        [TestMethod]
        [DataRow("", 12, Name_is_invalid)]
        [DataRow(null, 1.20, Name_is_invalid)]
        [DataRow("Test", -1, Capacity_is_invalid)]
        [DataRow("Test", 0, Capacity_is_invalid)]
        public void Throws_error_creating_invalid_group(string name, double amps, string expectedMessage)
        {
            Action action = ()
                => new Group(Name.Create(name), Capacity.InAmps(amps));

            action
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(expectedMessage);
        }

        [TestMethod]
        [DataRow("Station", 20, 30, 40)]
        [DataRow("Station", 10)]
        public void Able_to_add_new_station_to_group(string name, params double[] connectorCapacities)
        {
            var group = Fakes.BuildGroup(maxCapacity: 100);
            var configuration = new StationConfiguration(name, connectorCapacities);
            group.AddStation(configuration);

            group.ChargeStations
                .First().Name.Value
                .Should()
                .Be(name);

            group.ChargeStations.First()
                .Connectors
                .Should()
                .HaveCount(connectorCapacities.Count());

            group.AllocatedCapacity().Value
                .Should()
                .Be(connectorCapacities.Sum());
        }

        [TestMethod]
        public void Able_to_remove_station_from_the_group()
        {
            var group = Fakes
                .BuildGroup(maxCapacity: 100)
                .WithStation(connectorCapacities: new double[] { 20 });

            var stationId = group.ChargeStations.First().Id;

            group.RemoveStation(stationId);

            group.AllocatedCapacity().Value
                .Should()
                .Be(0);
        }

        [TestMethod]
        public void Throw_error_on_remove_station_that_is_not_part_of_the_group()
        {
            var group = Fakes
                .BuildGroup(maxCapacity: 100);

            Action action = ()
                => group.RemoveStation(Guid.NewGuid());

            action
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(Station_Not_Found);
        }

        [TestMethod]
        [DataRow(Name_is_invalid, "", 10, 20, 30)]
        [DataRow(Capacity_is_invalid, "InvalidStation", -20, 30, 40)]
        [DataRow(Station_requires_connectors, "InvalidStation")]
        public void Throw_error_adding_invalid_station_to_group(
            string expectedMessage, string name, params double[] connectorCapacities)
        {
            var group = Fakes.BuildGroup(maxCapacity: 100);

            Action action = () =>
            {
                var configuration = new StationConfiguration(name, connectorCapacities);
                group.AddStation(configuration);
            };

            action
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(expectedMessage);
        }

        [TestMethod]
        [DataRow(50, 10, 20, 30)]
        [DataRow(1000, 200, 300, 500.3)]
        public void Throw_error_adding_station_that_exeeds_group_capacity(
            double groupCapacity, params double[] connectorCapacities)
        {
            var group = Fakes.BuildGroup(maxCapacity: groupCapacity);

            var configuration = new StationConfiguration("Station", connectorCapacities);

            Action action = () => group.AddStation(configuration);

            action
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(Insuficient_Group_Capacity);
        }

        [TestMethod]
        [DataRow(50, 2)]
        [DataRow(50, 0.32)]
        [DataRow(1230, 1200)]
        public void Able_to_add_connector_to_station(double groupCapacity, double connectorCapacity)
        {
            var group = Fakes
                .BuildGroup(maxCapacity: groupCapacity)
                .WithStation(connectorCapacities: new double[] { 10, 20 });

            var groupAllocatedCapacity_beforeAdding =
                group.AllocatedCapacity();

            var stationId = group
                .ChargeStations
                .First().Id;

            group.AddConnectorToStation(stationId, connectorCapacity);

            var groupAllocatedCapacity_afterAdding =
                group.AllocatedCapacity();

            groupAllocatedCapacity_afterAdding.Value
               .Should()
               .Be(groupAllocatedCapacity_beforeAdding.Value + connectorCapacity);
        }

        [TestMethod]
        public void Throws_error_adding_connectors_to_station_not_part_of_group()
        {
            var group = Fakes
              .BuildGroup();

            Action addConnectorsAction = ()
                => group.AddConnectorToStation(Guid.NewGuid(), 50);

            addConnectorsAction
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(Station_Not_Found);
        }

        [TestMethod]
        [DataRow(-2)]
        [DataRow(0)]
        public void Throws_error_adding_connectors_to_station_invalid_capacity_value(double connectorCapacity)
        {
            var group = Fakes
                .BuildGroup()
                .WithStation(connectorCapacities: new double[] { 10, 20 });

            var stationId = group
                .ChargeStations
                .First().Id;

            Action addConnectorsAction = ()
                => group.AddConnectorToStation(stationId, connectorCapacity);

            addConnectorsAction
                .Should()
                .Throw<DomainOperationException>()
                .WithMessage(Capacity_is_invalid);
        }

        [TestMethod]
        [DataRow(100, 120)]
        [DataRow(10, 20)]
        [DataRow(50, 100)]
        public void Throws_error_adding_connectors_to_station_exceeds_group_capacity(
            double maxCapacity, double connectorCapacity)
        {
            var group = Fakes
                .BuildGroup(maxCapacity: maxCapacity)
                .WithStation(connectorCapacities: new double[] { 10 });

            var stationId = group
                .ChargeStations
                .First().Id;

            Action addConnectorsAction = ()
                => group.AddConnectorToStation(stationId, connectorCapacity);

            addConnectorsAction.Should()
                .Throw<DomainOperationException>()
                .WithMessage(Insuficient_Group_Capacity);
        }
    }
}