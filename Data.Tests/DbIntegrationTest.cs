﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Data.Tests
{
    public class DbIntegrationTest
    {
        protected IDbContextTransaction _transaction;
        protected GroupsDbContext _context;

        [TestInitialize]
        public virtual void Initialize()
        {
            SetUpContext();
            _transaction = _context.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _transaction.Rollback();
        }

        private void SetUpContext()
        {
            var services = new ServiceCollection();
            services.AddDbContext<GroupsDbContext>(p =>
            {
                p.UseSqlServer(DbSettings.ConnectionString);
                p.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });
            var provider = services.BuildServiceProvider();
            _context = provider.GetService<GroupsDbContext>();
        }
    }
}