﻿using Application.Repositories;
using Data.Exceptions;
using Domain;
using Domain.Tests;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Data.Tests
{
    [TestCategory("Integration")]
    [TestClass]
    public class RepositoryTests
        : DbIntegrationTest
    {
        private IRepository _repo;

        [TestMethod]
        public async Task Able_to_create_new_group()
        {
            var group = Fakes.BuildGroup("IntegrationTest_Group1", 1000);
            await _repo.Save(group);

            var savedGroup = await _repo.Get(group.Id);

            savedGroup
                .Should()
                .NotBeNull();
            savedGroup.Name.Value
                .Should()
                .Be("IntegrationTest_Group1");
        }

        [TestMethod]
        public async Task Able_to_update_group()
        {
            var group = Fakes.BuildGroup("IntegrationTest_Group1", 1000);
            await _repo.Save(group);
            group.ChangeGroupCapacity(Capacity.InAmps(2000));
            await _repo.Save(group);

            var updatedGroup = await _repo.Get(group.Id);

            updatedGroup.MaxCapacity.Value
                .Should()
                .Be(2000);
        }

        [TestMethod]
        public async Task Able_to_remove_group_cascade()
        {
            var id = new Guid("9BDC9634-5723-45FD-8919-99CDEF99789D");
            await _repo.Delete(id);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                async () => await _repo.Get(id));
        }

        [TestInitialize]
        public override void Initialize()
        {
            base.Initialize();
            _repo = new Repository(_context);
        }
    }
}