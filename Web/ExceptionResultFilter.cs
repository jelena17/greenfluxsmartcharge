﻿using Data.Exceptions;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Web
{
    public class ExceptionResultFilter :
       IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            context.Result = CreateResult(context.Exception);
        }

        private IActionResult CreateResult(Exception exception)
        {
            if (exception is EntityNotFoundException)
                return new NotFoundObjectResult(exception.Message);

            if (exception is DomainOperationException domain)
                return new BadRequestObjectResult(new { domain.Message, details = domain.Details });
            else
                return new ObjectResult(exception.Message) { StatusCode = 500 };
        }
    }
}