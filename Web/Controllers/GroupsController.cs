﻿using Application;
using Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly IQuery<IEnumerable<Group>> _allGroupsQuery;
        private readonly IQuery<Guid, Group> _groupQuery;
        private readonly ICommand<CreateGroupRequest> _createGroupCommand;
        private readonly ICommand<CreateStationRequest> _createStationCommand;
        private readonly ICommand<CreateConnectorRequest> _addConnectorCommand;
        private readonly ICommand<RemoveConnectorRequest> _removeConnectorCommand;
        private readonly ICommand<RemoveStationRequest> _removeStationCommand;
        private readonly ICommand<Guid> _removeGroupCommand;

        public GroupsController(
            IQuery<IEnumerable<Group>> allGroupsQuery,
            IQuery<Guid, Group> groupQuery,
            ICommand<CreateGroupRequest> createGroupCommand,
            ICommand<CreateStationRequest> createStationCommand,
            ICommand<CreateConnectorRequest> addConnectorCommand,
            ICommand<RemoveConnectorRequest> removeConnectorCommand,
            ICommand<RemoveStationRequest> removeStationCommand,
            ICommand<Guid> removeGroupCommand)
        {
            _allGroupsQuery = allGroupsQuery;
            _groupQuery = groupQuery;
            _createGroupCommand = createGroupCommand;
            _createStationCommand = createStationCommand;
            _addConnectorCommand = addConnectorCommand;
            _removeConnectorCommand = removeConnectorCommand;
            _removeStationCommand = removeStationCommand;
            _removeGroupCommand = removeGroupCommand;
        }

        [HttpGet]
        [Route("api/groups")]
        public async Task<IActionResult> GetAllGroups()
        {
            var result = await _allGroupsQuery.Execute();
            return Ok(result);
        }

        [HttpGet]
        [Route("api/groups/{id}")]
        public async Task<IActionResult> GetGroup(
            [FromRoute] Guid id)
        {
            var result = await _groupQuery.Execute(id);
            return Ok(result);
        }

        [HttpPost]
        [Route("api/groups")]
        public async Task<IActionResult> CreateGroup(
            [FromBody] CreateGroupRequest request)
        {
            await _createGroupCommand.Execute(request);
            return Created("", new { });
        }

        [HttpPost]
        [Route("api/groups/{groupId}/stations")]
        public async Task<IActionResult> CreateStation(
            [FromRoute] Guid groupId, [FromBody] CreateStationRequest request)
        {
            request.GroupId = groupId;
            await _createStationCommand.Execute(request);
            return Created("", new { });
        }

        [HttpPost]
        [Route("api/groups/{groupId}/stations/{stationId}/connectors")]
        public async Task<IActionResult> AddConnector(
            [FromRoute] Guid groupId, [FromRoute] Guid stationId,
            [FromBody] CreateConnectorRequest request)
        {
            request.GroupId = groupId;
            request.StationId = stationId;

            await _addConnectorCommand.Execute(request);
            return Created("", new { });
        }

        [HttpDelete]
        [Route("api/groups/{groupId}/stations/{stationId}/connectors")]
        public async Task<IActionResult> RemoveConnector(
            [FromRoute] Guid groupId, [FromRoute] Guid stationId,
            [FromBody] RemoveConnectorRequest request)
        {
            request.GroupId = groupId;
            request.StationId = stationId;

            await _removeConnectorCommand.Execute(request);
            return Ok();
        }

        [HttpDelete]
        [Route("api/groups/{groupId}")]
        public async Task<IActionResult> RemoveGroup([FromRoute] Guid groupId)
        {
            await _removeGroupCommand.Execute(groupId);
            return Ok();
        }

        [HttpDelete]
        [Route("api/groups/{groupId}/stations/{stationId}")]
        public async Task<IActionResult> RemoveStation(
            [FromRoute] Guid groupId,
            [FromRoute] Guid stationId)
        {
            var request = new RemoveStationRequest
            {
                GroupId = groupId,
                StationId = stationId
            };
            await _removeStationCommand.Execute(request);
            return Ok();
        }
    }
}