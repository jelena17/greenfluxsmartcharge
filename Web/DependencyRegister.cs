﻿using Application;
using Application.Repositories;
using Data;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Web
{
    public static class DependencyRegister
    {
        public static void Register(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IRepository, Repository>();
            services.AddScoped<IQuery<IEnumerable<Group>>, AllGroupsQuery>();
            services.AddScoped<IQuery<Guid, Group>, GroupQuery>();
            services.AddScoped<ICommand<CreateGroupRequest>, CreateGroupCommand>();
            services.AddScoped<ICommand<CreateStationRequest>, CreateStationCommand>();
            services.AddScoped<ICommand<CreateConnectorRequest>, AddConnetorCommand>();
            services.AddScoped<ICommand<Guid>, RemoveGroupCommand>();
            services.AddScoped<ICommand<RemoveConnectorRequest>, RemoveConnetorCommand>();
            services.Decorate<ICommand<CreateConnectorRequest>, AddConnectorWithSuggestionsOnInsufficientCapacityError>();
            services.AddScoped<ICommand<RemoveStationRequest>, RemoveStationCommand>();
            services.AddDbContext<GroupsDbContext>(optionsBuilder =>
                optionsBuilder
                .UseSqlServer(configuration["ConnectionStrings:GroupsDb"])
            );
        }
    }
}