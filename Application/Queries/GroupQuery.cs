﻿using Application.Repositories;
using Domain;
using System;
using System.Threading.Tasks;

namespace Application
{
    public class GroupQuery
        : IQuery<Guid, Group>
    {
        private readonly IRepository _reporitory;

        public GroupQuery(IRepository reporitory)
        {
            _reporitory = reporitory;
        }

        public async Task<Group> Execute(Guid request)
        {
            return await
             _reporitory.Get(request);
        }
    }
}