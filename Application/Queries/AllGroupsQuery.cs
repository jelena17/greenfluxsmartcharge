﻿using Application.Repositories;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application
{
    public class AllGroupsQuery
        : IQuery<IEnumerable<Group>>
    {
        private readonly IRepository _reporitory;

        public AllGroupsQuery(IRepository reporitory)
        {
            _reporitory = reporitory;
        }

        public Task<IEnumerable<Group>> Execute()
            => _reporitory.GetAll();
    }
}