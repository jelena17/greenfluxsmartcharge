﻿using Application.Repositories;
using System.Threading.Tasks;

namespace Application
{
    public class AddConnetorCommand
        : ICommand<CreateConnectorRequest>
    {
        private readonly IRepository _repository;

        public AddConnetorCommand(IRepository repo)
        {
            _repository = repo;
        }

        public async Task Execute(CreateConnectorRequest request)
        {
            var group = await
              _repository.Get(request.GroupId);

            group.AddConnectorToStation(
                   request.StationId, request.ConnectorCapacity);

            await _repository.Save(group);
        }
    }
}