﻿using Application.Repositories;
using System.Threading.Tasks;

namespace Application
{
    public class RemoveStationCommand
        : ICommand<RemoveStationRequest>
    {
        private readonly IRepository _repository;

        public RemoveStationCommand(IRepository repository)
        {
            _repository = repository;
        }

        public async Task Execute(RemoveStationRequest request)
        {
            var group = await
                _repository.Get(request.GroupId);

            group.RemoveStation(request.StationId);

            await _repository.Save(group);
        }
    }
}