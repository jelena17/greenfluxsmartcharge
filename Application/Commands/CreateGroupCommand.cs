﻿using Application.Repositories;
using Domain;
using System.Threading.Tasks;

namespace Application
{
    public class CreateGroupCommand
        : ICommand<CreateGroupRequest>
    {
        private readonly IRepository _repository;

        public CreateGroupCommand(IRepository repository)
        {
            _repository = repository;
        }

        public Task Execute(CreateGroupRequest request)
        {
            var groupCapacity = Capacity.InAmps(request.CapacityInAmps);
            var name = new Name(request.Name);
            var newGroup = new Group(name, groupCapacity);

            return _repository.Save(newGroup);
        }
    }
}