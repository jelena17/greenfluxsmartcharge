﻿using Domain;
using Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class AddConnectorWithSuggestionsOnInsufficientCapacityError
        : ICommand<CreateConnectorRequest>
    {
        private ICommand<CreateConnectorRequest> _command;
        private IQuery<Guid, Group> _query;

        public AddConnectorWithSuggestionsOnInsufficientCapacityError(
            ICommand<CreateConnectorRequest> command,
            IQuery<Guid, Group> query)
        {
            _command = command;
            _query = query;
        }

        public async Task Execute(CreateConnectorRequest request)
        {
            try
            {
                await _command.Execute(request);
            }
            catch (DomainOperationException e)
            {
                if (e.Message != ErrorMessage.Insuficient_Group_Capacity)
                    throw;

                var suggestions = await GenerateSuggestions(request, e);
                e.Details = suggestions;
                throw;
            }
        }

        private async Task<object> GenerateSuggestions(
            CreateConnectorRequest request, DomainOperationException e)
        {
            var group = await _query.Execute(request.GroupId);
            var targetCapacity = Capacity.InAmps(request.ConnectorCapacity);
            var finder = new MinimumNumberOfConnectors_GroupCapacitySubSetFinder();

            List<List<StationConnector>> results = finder
                .FindConnectorSubSets(group, targetCapacity);

            return MapToResponse(results);
        }

        private object MapToResponse(List<List<StationConnector>> results)
        {
            return results.Select(combination => new
            {
                Combination = combination.Select(item => new
                {
                    Station = new
                    {
                        Id = item.Station.Id,
                        Name = item.Station.Name.Value
                    },
                    Connector = new
                    {
                        Id = item.Connector.Id,
                        MaxCapacity = item.Connector.MaxCapacity.Value
                    }
                })
            });
        }
    }
}