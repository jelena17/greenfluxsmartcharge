﻿using Application.Repositories;
using System;
using System.Threading.Tasks;

namespace Application
{
    public class RemoveGroupCommand
        : ICommand<Guid>
    {
        private readonly IRepository _repository;

        public RemoveGroupCommand(IRepository repository)
        {
            _repository = repository;
        }

        public Task Execute(Guid request)
            => _repository.Delete(request);
    }
}