﻿using Application.Repositories;
using System.Threading.Tasks;

namespace Application
{
    public class RemoveConnetorCommand
        : ICommand<RemoveConnectorRequest>
    {
        private readonly IRepository _repository;

        public RemoveConnetorCommand(IRepository repository)
        {
            _repository = repository;
        }

        public async Task Execute(RemoveConnectorRequest request)
        {
            var group = await
              _repository.Get(request.GroupId);

            group.RemoveConnectorFromStation(
                request.StationId, request.ConnectorId);

            await _repository.Save(group);
        }
    }
}