﻿using Application.Repositories;
using Domain;
using System.Threading.Tasks;

namespace Application
{
    public class CreateStationCommand
        : ICommand<CreateStationRequest>
    {
        private readonly IRepository _repository;

        public CreateStationCommand(IRepository repository)
        {
            _repository = repository;
        }

        public async Task Execute(CreateStationRequest request)
        {
            var group = await
               _repository.Get(request.GroupId);

            var stationConfiguration =
                new StationConfiguration(request.Name, request.ConnectorCapacities);

            group.AddStation(stationConfiguration);

            await _repository.Save(group);
        }
    }
}