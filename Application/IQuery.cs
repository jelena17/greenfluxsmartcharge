﻿using System.Threading.Tasks;

namespace Application
{
    public interface IQuery<TRequest, TResult>
    {
        Task<TResult> Execute(TRequest request);
    }

    public interface IQuery<TResult>
    {
        Task<TResult> Execute();
    }
}