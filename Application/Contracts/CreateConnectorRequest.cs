﻿using System;
using System.Text.Json.Serialization;

namespace Application
{
    public class CreateConnectorRequest
    {
        [JsonIgnore]
        public Guid GroupId { get; set; }

        [JsonIgnore]
        public Guid StationId { get; set; }

        [NotDefault]
        public double ConnectorCapacity { get; set; }
    }
}