﻿using System;
using System.Text.Json.Serialization;

namespace Application
{
    public class RemoveConnectorRequest
    {
        [JsonIgnore]
        public Guid GroupId { get; set; }

        [JsonIgnore]
        public Guid StationId { get; set; }

        [NotDefault]
        public int ConnectorId { get; set; }
    }
}