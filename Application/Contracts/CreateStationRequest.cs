﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Application
{
    public class CreateStationRequest
    {
        [JsonIgnore]
        public Guid GroupId { get; set; }

        [NotDefault]
        public string Name { get; set; }

        [NotDefault]
        public IEnumerable<double> ConnectorCapacities { get; set; }
    }
}