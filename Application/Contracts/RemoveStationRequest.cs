﻿using System;
using System.Text.Json.Serialization;

namespace Application
{
    public class RemoveStationRequest
    {
        [JsonIgnore]
        public Guid GroupId { get; set; }

        [JsonIgnore]
        public Guid StationId { get; set; }
    }
}