﻿namespace Application
{
    public class CreateGroupRequest
    {
        [NotDefault]
        public string Name { get; set; }

        [NotDefault]
        public double CapacityInAmps { get; set; }
    }
}