﻿using Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IRepository
    {
        Task<IEnumerable<Group>> GetAll();

        Task<Group> Get(Guid id);

        Task Save(Group group);

        Task Delete(Guid id);
    }
}