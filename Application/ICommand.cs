﻿using System.Threading.Tasks;

namespace Application
{
    public interface ICommand<T>
    {
        Task Execute(T request);
    }
}