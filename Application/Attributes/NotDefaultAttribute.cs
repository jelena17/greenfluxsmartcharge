﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Application
{
    public class NotDefaultAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            var type = value.GetType();

            if (type == typeof(string))
                return !string.IsNullOrWhiteSpace((string)value)
                    && !string.IsNullOrEmpty((string)value);

            if (type.IsArray)
                return ((ICollection)value).Count > 0;

            object defaultValue = Activator.CreateInstance(type);
            return defaultValue != value;
        }
    }
}