﻿using System;

namespace Domain
{
    public abstract class Entity
    {
        public Guid Id { get; protected set; }

        protected Entity(Guid id)
        {
            Id = id;
        }

        protected Entity() : this(new Guid())
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is Entity)
                return Id == (obj as Entity).Id;
            return false;
        }

        public override int GetHashCode()
            => Id.GetHashCode();
    }
}