﻿using System;

namespace Domain
{
    public class DomainOperationException
        : Exception
    {
        public object Details { get; set; }

        public DomainOperationException(string message) : base(message)
        {
        }
    }
}