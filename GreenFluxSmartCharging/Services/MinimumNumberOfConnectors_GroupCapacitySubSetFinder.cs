﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Services
{
    public class MinimumNumberOfConnectors_GroupCapacitySubSetFinder
    {
        private readonly List<List<StationConnector>> _combinations;
        private List<StationConnector> _connectors;
        private int _conectorsCount;

        public MinimumNumberOfConnectors_GroupCapacitySubSetFinder()
        {
            _combinations = new List<List<StationConnector>>();
        }

        public List<List<StationConnector>> FindConnectorSubSets(Group group, Capacity targetCapacity)
        {
            _connectors = FilterEqualOrLowerConnectorCapacities(group, targetCapacity);
            _conectorsCount = _connectors.Count;

            for (int currentIdx = 0; currentIdx < _conectorsCount; currentIdx++)
            {
                var currentCombination = new List<StationConnector>();
                var currentConnector = _connectors[currentIdx];

                FindCombinationForTargetCapacity(
                    currentIdx,
                    targetCapacity,
                    currentConnector,
                    currentCombination);
            }
            return _combinations;
        }

        private List<StationConnector> FilterEqualOrLowerConnectorCapacities(Group group, Capacity targetCapacity)
        {
            return group.ChargeStations
                .SelectMany(station => station.Connectors
                     .Where(connector => connector.MaxCapacity <= targetCapacity)
                     .Select(connector => new StationConnector(station, connector)))
                .OrderByDescending(connector => connector.MaxCapacity)
                .ToList();
        }

        private void FindCombinationForTargetCapacity(
            int index,
            Capacity targetCapacity,
            StationConnector current,
            List<StationConnector> currentCombinations)
        {
            if (IsMatch(targetCapacity, current))
            {
                currentCombinations.Add(current);
                if (IsAmongOptimalCombinations(currentCombinations))
                {
                    HandleMultipleMatches(index + 1, targetCapacity, currentCombinations);
                    _combinations.Add(currentCombinations);
                    return;
                }
            }

            if (IsLastInCollection(index))
                return;

            var newTargetCapacity = targetCapacity - current.MaxCapacity;

            if (newTargetCapacity > Capacity.Default)
                currentCombinations.Add(current);
            else
                newTargetCapacity = targetCapacity;

            FindCombinationForTargetCapacity(
                index + 1,
                newTargetCapacity,
                _connectors[index + 1],
                currentCombinations);
        }

        private void HandleMultipleMatches(int index, Capacity target, List<StationConnector> currentCombinations)
        {
            if (currentCombinations.Count != 1)
                while (_conectorsCount > index && IsMatch(target, _connectors[index]))
                {
                    var alternateCombination = new List<StationConnector>(currentCombinations.SkipLast(1));
                    alternateCombination.Add(_connectors[index]);
                    _combinations.Add(alternateCombination);
                    index++;
                }
        }

        private bool IsAmongOptimalCombinations(List<StationConnector> currentCombinations)
            => !_combinations.Any() || _combinations.Last().Count == currentCombinations.Count;

        private bool IsLastInCollection(int currentIndex)
            => _conectorsCount == currentIndex + 1;

        private static bool IsMatch(Capacity targetCapacity, StationConnector current)
            => current.MaxCapacity == targetCapacity;
    }
}