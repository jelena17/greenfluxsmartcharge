﻿namespace Domain.Services
{
    public class StationConnector
    {
        public ChargeStation Station { get; private set; }
        public Connector Connector { get; private set; }
        public Capacity MaxCapacity => Connector.MaxCapacity;

        public StationConnector(ChargeStation station, Connector connector)
        {
            Station = station;
            Connector = connector;
        }
    }
}