﻿using Domain.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Extensions
{
    public static class CapacityExtensions
    {
        public static Capacity Sum(this IEnumerable<Capacity> items)
        {
            if (!items.Any())
                return Capacity.Default;

            var sum = items.Sum(item => item.Value);
            return Capacity.Create(sum, items.First().Unit);
        }
    }
}