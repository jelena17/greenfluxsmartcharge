﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Domain
{
    public abstract class ValueObject<T>
     where T : ValueObject<T>
    {
        public static bool operator ==(ValueObject<T> obj1, ValueObject<T> obj2)
            => obj1.Equals(obj2);

        public static bool operator !=(ValueObject<T> obj1, ValueObject<T> obj2)
            => !obj1.Equals(obj2);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (obj.GetType() != this.GetType())
                return false;

            return PropertiesEquals((T)obj);
        }

        public override int GetHashCode()
        {
            var hash = 11;
            foreach (var field in GetProperties(typeof(T)))
                hash *= field.GetValue(this).GetHashCode();
            return hash;
        }

        private bool PropertiesEquals(T other)
        {
            var props = GetProperties(typeof(T));

            foreach (var prop in props)
            {
                var propValueOfThis = prop.GetValue(this);
                var propValueOfOther = prop.GetValue(other);

                if (!propValueOfThis.Equals(propValueOfOther))
                    return false;
            }
            return true;
        }

        private static IEnumerable<PropertyInfo> GetProperties(Type type)
        {
            var properties = new List<PropertyInfo>();

            while (type != typeof(object))
            {
                var typeProperties = type.GetProperties(
                    BindingFlags.Instance |
                    BindingFlags.Public);

                properties.AddRange(typeProperties);
                type = type.BaseType;
            }
            return properties;
        }
    }
}