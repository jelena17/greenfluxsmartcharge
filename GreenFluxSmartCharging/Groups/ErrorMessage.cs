﻿namespace Domain
{
    public static class ErrorMessage
    {
        public const string Insuficient_Group_Capacity = "Not enough group capacity.";
        public const string Station_Not_Found = "Station not found in group.";
        public const string Connector_Not_Found = "Connector not found in station.";
        public const string Insuficient_Station_Capacity = "Not enough group capacity.";
        public const string Station_requires_connectors = "Station can not be initialized without connectors.";
        public const string Name_is_invalid = "Name can not be empty.";
        public const string Capacity_is_invalid = "Capacity can not be 0 or negative number.";
    }
}