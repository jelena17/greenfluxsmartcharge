﻿using System.Collections.Generic;

namespace Domain
{
    public class StationConfiguration
    {
        public Name Name { get; private set; }
        public IEnumerable<double> ConnectorCapacities { get; private set; }

        public StationConfiguration(string name, IEnumerable<double> connectorCapacities)
        {
            Name = Name.Create(name);
            ConnectorCapacities = connectorCapacities;
        }
    }
}