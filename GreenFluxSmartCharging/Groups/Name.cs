﻿namespace Domain
{
    public class Name
    {
        public string Value { get; private set; }

        public Name(string value)
        {
            if (string.IsNullOrEmpty(value?.Trim()))
                throw new DomainOperationException(ErrorMessage.Name_is_invalid);
            else
                Value = value;
        }

        public static Name Create(string name)
            => new Name(name);
    }
}