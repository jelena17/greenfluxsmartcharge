﻿using Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Group
        : Entity
        , IMeasurableCapacity
    {
        public Name Name { get; private set; }
        public Capacity MaxCapacity { get; private set; }
        public IReadOnlyList<ChargeStation> ChargeStations => _chargeStations;
        private readonly List<ChargeStation> _chargeStations;

        public Group(Name name, Capacity capacity)
            : this()
        {
            Id = Guid.NewGuid();
            Name = name;
            MaxCapacity = capacity;
        }

        private Group() : base()
        {
            _chargeStations = new List<ChargeStation>();
        }

        public void ChangeName(Name name)
            => Name = name;

        public void ChangeGroupCapacity(Capacity capacity)
        {
            if (AllocatedCapacity() > capacity)
                throw new DomainOperationException(ErrorMessage.Insuficient_Group_Capacity);

            MaxCapacity = capacity;
        }

        // I could use station name and connector capacities but station configuration could hold in
        // future all additional data needed for instantiating ChargeStation in dedicated factory method
        public void AddStation(StationConfiguration stationConfig)
        {
            ChargeStation station = CreateStation(stationConfig);

            ValidateCapacityCanBeAddedToGroup(station.AllocatedCapacity());

            _chargeStations.Add(station);
        }

        public void RemoveStation(Guid stationId)
        {
            var station = GetStation(stationId);
            _chargeStations.Remove(station);
        }

        // connectorConfiguration class could potentially hold additional connector data for current
        // scope and simplicity was not introduced.
        public void AddConnectorToStation(Guid stationId, double connectorCapacity)
        {
            ValidateCapacityCanBeAddedToGroup(
                Capacity.InAmps(connectorCapacity));

            ChargeStation station = GetStation(stationId);
            station.AddConnector(connectorCapacity);
        }

        public void RemoveConnectorFromStation(Guid stationId, int connectorId)
        {
            var station = GetStation(stationId);
            station.RemoveConnector(connectorId);
        }

        public Capacity AllocatedCapacity()
        {
            return _chargeStations
                .Select(station => station.AllocatedCapacity())
                .Sum();
        }

        private ChargeStation GetStation(Guid stationId)
        {
            var station = _chargeStations
                .FirstOrDefault(station => station.Id == stationId);

            if (station == default)
                throw new DomainOperationException(ErrorMessage.Station_Not_Found);

            return station;
        }

        private void ValidateCapacityCanBeAddedToGroup(Capacity capacityToBeAdded)
        {
            var groupAllocatedCapacity = AllocatedCapacity();

            if (MaxCapacity < Capacity.Sum(groupAllocatedCapacity, capacityToBeAdded))
                throw new DomainOperationException(ErrorMessage.Insuficient_Group_Capacity);
        }

        private static ChargeStation CreateStation(StationConfiguration stationConfig)
           => new ChargeStation(stationConfig.Name, stationConfig.ConnectorCapacities);
    }
}