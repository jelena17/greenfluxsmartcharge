﻿namespace Domain
{
    public interface IMeasurableCapacity
    {
        Capacity AllocatedCapacity();
    }
}