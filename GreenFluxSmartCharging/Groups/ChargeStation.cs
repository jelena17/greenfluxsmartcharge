﻿using Domain.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class ChargeStation
        : Entity
        , IMeasurableCapacity
    {
        public Name Name { get; private set; }
        public IEnumerable<Connector> Connectors => _connectors.ToList();
        private readonly LimitedCollection<Connector> _connectors;
        private const short max_connector_count = 5;

        public ChargeStation(
            Name name,
            IEnumerable<double> connectorCapacities)
            : this()
        {
            Name = name;
            SetConnectors(connectorCapacities);
        }

        private ChargeStation() : base()
        {
            _connectors = new LimitedCollection<Connector>(max_connector_count);
        }

        public void ChangeName(Name name)
            => Name = name;

        public void AddConnector(double maxAmps)
        {
            if (_connectors.IsFull)
                throw new DomainOperationException(ErrorMessage.Insuficient_Station_Capacity);

            int position = GenerateConnectorId();
            var connector = new Connector(position, maxAmps);

            _connectors.Add(connector);
        }

        public void RemoveConnector(int connectorId)
        {
            var isInCollection = _connectors
                 .TryFind((Connector) => Connector.Id == connectorId, out Connector connector);

            if (!isInCollection)
                throw new DomainOperationException(ErrorMessage.Connector_Not_Found);

            _connectors.Remove(connector);
        }

        public Capacity AllocatedCapacity()
        {
            return _connectors
                .Select(con => con.AllocatedCapacity())
                .Sum();
        }

        private void SetConnectors(IEnumerable<double> connectorCapacities)
        {
            if (connectorCapacities == null || !connectorCapacities.Any())
                throw new DomainOperationException(ErrorMessage.Station_requires_connectors);

            foreach (var connector in connectorCapacities)
                AddConnector(connector);
        }

        private int GenerateConnectorId()
        {
            return _connectors
                .Select((connector, idx) => new { connector, index = idx + 1 })
                .Where(pair => pair.connector.Id != pair.index)
                .Select(pair => pair.index)
                .DefaultIfEmpty(_connectors.UsedCapacity + 1)
                .FirstOrDefault();
        }
    }
}