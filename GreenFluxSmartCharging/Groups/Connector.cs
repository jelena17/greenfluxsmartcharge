﻿namespace Domain
{
    public class Connector
        : ValueObject<Connector>
        , IMeasurableCapacity
    {
        public int Id { get; private set; }
        public Capacity MaxCapacity { get; private set; }

        public Connector(int id, double maxAmps)
        {
            Id = id;
            SetMaxAmps(maxAmps);
        }

        private Connector()
        {
        }

        public void SetMaxAmps(double maxAmps)
            => MaxCapacity = Capacity.InAmps(maxAmps);

        public Capacity AllocatedCapacity()
            => MaxCapacity;
    }
}