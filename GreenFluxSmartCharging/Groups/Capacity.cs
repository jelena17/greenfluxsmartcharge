﻿using System;

namespace Domain
{
    public class Capacity
        : ValueObject<Capacity>, IComparable<Capacity>
    {
        public double Value { get; private set; }
        public Unit Unit { get; private set; }

        private Capacity(double value, Unit unit)
        {
            Value = value;
            Unit = unit;
        }

        public static Capacity Default
            => new Capacity(0, Unit.Amp);

        public static Capacity InAmps(double value)
           => Create(value, Unit.Amp);

        public static Capacity Create(double value, Unit unit)
        {
            if (value <= 0)
                throw new DomainOperationException("Capacity can not be 0 or negative number.");

            return new Capacity(value, unit);
        }

        public static bool AreOfSameUnit(Capacity left, Capacity other)
            => left.Unit.Equals(other.Unit);

        public static Capacity Sum(Capacity left, Capacity right)
        {
            ValidateArOfSameUnit(left, right);

            var sumValue = left.Value + right.Value;
            return new Capacity(sumValue, left.Unit);
        }

        public static Capacity operator +(Capacity left, Capacity right)
            => Sum(left, right);

        public static Capacity operator -(Capacity left, Capacity right)
        {
            ValidateArOfSameUnit(left, right);

            var value = left.Value - right.Value;
            return new Capacity(value, left.Unit);
        }

        public static bool operator >(Capacity left, Capacity right)
        {
            ValidateArOfSameUnit(left, right);
            return left.Value > right.Value;
        }

        public static bool operator <(Capacity left, Capacity right)
        {
            ValidateArOfSameUnit(left, right);
            return left.Value < right.Value;
        }

        public static bool operator >=(Capacity left, Capacity right)
        {
            ValidateArOfSameUnit(left, right);
            return left.Value >= right.Value;
        }

        public static bool operator <=(Capacity left, Capacity right)
        {
            ValidateArOfSameUnit(left, right);
            return left.Value <= right.Value;
        }

        private static void ValidateArOfSameUnit(Capacity left, Capacity right)
        {
            if (!AreOfSameUnit(left, right))
                throw new ArgumentException("Unable to compare capacities of different units.");
        }

        public int CompareTo(Capacity other)
        {
            if (this > other)
                return 2;
            if (this < other)
                return -1;
            return 0;
        }
    }
}