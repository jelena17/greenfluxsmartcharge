﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class LimitedCollection<T>
        : ICollection<T>
    {
        private readonly List<T> _items;

        public LimitedCollection(
            int maxLength)
        {
            _items = new List<T>(maxLength);
        }

        public LimitedCollection(
           int maxLength,
           IEnumerable<T> items)
        {
            if (items == null)
                throw new DomainOperationException($"Collection initialization fails, items are not set.");

            if (maxLength < items.Count())
                throw new DomainOperationException($"Collection initialization allows maximum  of {maxLength} items.");

            _items = new List<T>(maxLength);
            _items.AddRange(items);
        }

        public static LimitedCollection<T> Default =>
              new LimitedCollection<T>(10);

        public bool IsFull =>
            UsedCapacity == TotalCapacity;

        public int TotalCapacity =>
           _items.Capacity;

        public int UsedCapacity =>
         _items.Where(x => x != null).Count();

        public int Count => _items.Count;

        public bool IsReadOnly => false;

        public void Add(T item)
        {
            if (IsFull)
                throw new DomainOperationException("Collection is full.");
            _items.Add(item);
        }

        public void Remove(T item)
        {
            var index = FindPosition(item);
            _items.RemoveAt(index);
        }

        public bool TryFind(Func<T, bool> filterMethod, out T item)
        {
            item = _items
                .Where(i => filterMethod(i))
                .FirstOrDefault();
            return !object.Equals(item, default(T));
        }

        private int FindPosition(T item)
        {
            var positon = _items.IndexOf(item);

            if (positon < 0)
                throw new ArgumentException("Item not found.");
            return positon;
        }

        public IEnumerator<T> GetEnumerator()
            => _items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();

        public void Clear()
            => _items.Clear();

        public bool Contains(T item)
            => _items.Contains(item);

        public void CopyTo(T[] array, int arrayIndex)
            => Array.Copy(_items.Skip(arrayIndex).ToArray(),
                array, _items.Skip(arrayIndex).Count());

        bool ICollection<T>.Remove(T item)
        {
            try
            {
                Remove(item);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}