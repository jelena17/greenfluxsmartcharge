## Architecture : 
1.	**Domain**
Contains domain objects Groups, Stations, Connectors, domain validation and services (find the subset of group connectors for targeted capacity)
2.	**Application** 
Defines and implements use-cases, queries and commands. 
3.	**Data** 
Implementation of repository defined in the application layer. EF is used, to create DB, please run migrations.
4.	**Web**
Contains endpoints that execute queries/commands.
5.	**Tests**
Unit tests to validate domain models and service logic. 
Integration tests validating database access layer.

