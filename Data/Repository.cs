﻿using Application.Repositories;
using Data.Exceptions;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data
{
    public class Repository : IRepository
    {
        private readonly GroupsDbContext _db;

        public Repository(GroupsDbContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<Group>> GetAll()
            => await _db.Groups.ToListAsync();

        public async Task<Group> Get(Guid id)
        {
            var group = await _db.Groups
                .Include(g => g.ChargeStations)
                .ThenInclude(s => s.Connectors)
                .ThenInclude(c => c.MaxCapacity)
                .SingleOrDefaultAsync(g => g.Id == id);

            if (group == default)
                throw new EntityNotFoundException($"Resource with id {id} not found.");

            return group;
        }

        public async Task Save(Group group)
        {
            await _db.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var group = await _db.Groups
                .SingleOrDefaultAsync(g => g.Id == id);

            if (group == default)
                throw new EntityNotFoundException($"Resource with id {id} not found.");

            _db.Groups.Remove(group);
            await _db.SaveChangesAsync();
        }
    }
}