﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Data
{
    public class GroupsDbContext
        : DbContext
    {
        public GroupsDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Group> Groups { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetUpGroup(modelBuilder);
            SetUpChargeStation(modelBuilder);
            SetUpConnector(modelBuilder);
        }

        private static void SetUpGroup(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>(group =>
                {
                    group.ToTable("Group");

                    group.OwnsOne(p => p.Name, name =>
                    {
                        name
                        .Property(p => p.Value)
                        .HasMaxLength(300)
                        .IsRequired()
                        .HasColumnName("Name");

                        name
                        .HasIndex(x => x.Value)
                        .IsUnique();
                    });
                    group.OwnsOne(group => group.MaxCapacity,
                        capacity =>
                        {
                            capacity
                                 .Property(p => p.Value)
                                 .HasColumnName("CapacityValue");
                            capacity
                                 .Property(p => p.Unit)
                                 .HasColumnName("CapacityUnit")
                                 .HasConversion(new EnumToStringConverter<Unit>(mappingHints:
                                 new ConverterMappingHints(300)));
                        });

                    group
                        .HasMany(group => group.ChargeStations)
                        .WithOne()
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }

        private static void SetUpChargeStation(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChargeStation>(station =>
                 {
                     station.OwnsOne(p => p.Name, name =>
                     {
                         name
                         .Property(p => p.Value)
                         .HasMaxLength(300)
                         .IsRequired()
                         .HasColumnName("Name");
                     });

                     station
                     .HasMany(x => x.Connectors)
                     .WithOne()
                     .HasForeignKey("ChargeStationId");
                 });
        }

        private static void SetUpConnector(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Connector>(connector =>
            {
                connector
                    .HasKey(new string[] { "Id", "ChargeStationId" });
                connector
                    .Property(p => p.Id)
                    .ValueGeneratedNever();

                connector.OwnsOne(c => c.MaxCapacity, capacity =>
                   {
                       capacity
                            .Property(p => p.Value)
                            .HasColumnName("CapacityValue");
                       capacity
                            .Property(p => p.Unit)
                            .HasColumnName("CapacityUnit")
                            .HasConversion(new EnumToStringConverter<Unit>(mappingHints:
                             new ConverterMappingHints(300)));
                   });
            });
        }
    }
}