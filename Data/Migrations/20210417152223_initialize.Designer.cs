﻿// <auto-generated />
using System;
using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Data.Migrations
{
    [DbContext(typeof(GroupsDbContext))]
    [Migration("20210417152223_initialize")]
    partial class initialize
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.4")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Domain.ChargeStation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid?>("GroupId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.ToTable("ChargeStation");
                });

            modelBuilder.Entity("Domain.Connector", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<Guid>("ChargeStationId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id", "ChargeStationId");

                    b.HasIndex("ChargeStationId");

                    b.ToTable("Connector");
                });

            modelBuilder.Entity("Domain.Group", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("Group");
                });

            modelBuilder.Entity("Domain.ChargeStation", b =>
                {
                    b.HasOne("Domain.Group", null)
                        .WithMany("ChargeStations")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.OwnsOne("Domain.Name", "Name", b1 =>
                        {
                            b1.Property<Guid>("ChargeStationId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<string>("Value")
                                .IsRequired()
                                .HasMaxLength(300)
                                .HasColumnType("nvarchar(300)")
                                .HasColumnName("Name");

                            b1.HasKey("ChargeStationId");

                            b1.ToTable("ChargeStation");

                            b1.WithOwner()
                                .HasForeignKey("ChargeStationId");
                        });

                    b.Navigation("Name");
                });

            modelBuilder.Entity("Domain.Connector", b =>
                {
                    b.HasOne("Domain.ChargeStation", null)
                        .WithMany("Connectors")
                        .HasForeignKey("ChargeStationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.OwnsOne("Domain.Capacity", "MaxCapacity", b1 =>
                        {
                            b1.Property<int>("ConnectorId")
                                .HasColumnType("int");

                            b1.Property<Guid>("ConnectorChargeStationId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<string>("Unit")
                                .IsRequired()
                                .HasColumnType("nvarchar(300)")
                                .HasColumnName("CapacityUnit");

                            b1.Property<double>("Value")
                                .HasColumnType("float")
                                .HasColumnName("CapacityValue");

                            b1.HasKey("ConnectorId", "ConnectorChargeStationId");

                            b1.ToTable("Connector");

                            b1.WithOwner()
                                .HasForeignKey("ConnectorId", "ConnectorChargeStationId");
                        });

                    b.Navigation("MaxCapacity");
                });

            modelBuilder.Entity("Domain.Group", b =>
                {
                    b.OwnsOne("Domain.Capacity", "MaxCapacity", b1 =>
                        {
                            b1.Property<Guid>("GroupId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<string>("Unit")
                                .IsRequired()
                                .HasColumnType("nvarchar(300)")
                                .HasColumnName("CapacityUnit");

                            b1.Property<double>("Value")
                                .HasColumnType("float")
                                .HasColumnName("CapacityValue");

                            b1.HasKey("GroupId");

                            b1.ToTable("Group");

                            b1.WithOwner()
                                .HasForeignKey("GroupId");
                        });

                    b.OwnsOne("Domain.Name", "Name", b1 =>
                        {
                            b1.Property<Guid>("GroupId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<string>("Value")
                                .IsRequired()
                                .HasMaxLength(300)
                                .HasColumnType("nvarchar(300)")
                                .HasColumnName("Name");

                            b1.HasKey("GroupId");

                            b1.HasIndex("Value")
                                .IsUnique()
                                .HasFilter("[Name] IS NOT NULL");

                            b1.ToTable("Group");

                            b1.WithOwner()
                                .HasForeignKey("GroupId");
                        });

                    b.Navigation("MaxCapacity");

                    b.Navigation("Name");
                });

            modelBuilder.Entity("Domain.ChargeStation", b =>
                {
                    b.Navigation("Connectors");
                });

            modelBuilder.Entity("Domain.Group", b =>
                {
                    b.Navigation("ChargeStations");
                });
#pragma warning restore 612, 618
        }
    }
}
