﻿using System;

namespace Data.Exceptions
{
    public class DataAcessException
       : Exception
    {
        public DataAcessException(string message) : base(message)
        {
        }
    }
}
