﻿namespace Data.Exceptions
{
    public class EntityNotFoundException
       : DataAcessException
    {
        public EntityNotFoundException(string message) : base(message)
        {
        }
    }
}